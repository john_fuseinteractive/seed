<?php

/**
 * Implementation of hook_seed_pre_sync().
 *
 * Performs some extremely simple code quality checking.
 */
function quality_seed_pre_sync($from_alias, $to_alias, $selection) {
  if(!drush_confirm(dt('Run code quality check?'))) {
    return;
  }

  $empty = array();

  $from = drush_sitealias_evaluate_path($from_alias, $empty);

  drush_log(dt('[Quality] Starting scan of !dir', array('!dir' => $from['root'])), 'ok');

  // Search for potentially uncommented dpm calls.
  // TODO: This doesn't care if the dpm is inside a block comment (/* */)
  drush_shell_exec("find {$from['root']} -type f ! -name '*\.txt' ! -name '*devel*' -print0 | xargs -0 grep -rn 'dpm(' | grep -v '//'");
  $dpm_results = drush_shell_exec_output();
  if ($dpm_count = count($dpm_results)) {
    drush_log(dt('Possibly uncommented dpm() calls:'), 'warning');
    seed_util_error_print($dpm_results);
  }

  // Search for JS files which use console but not window.console.
  // Old IE doesn't have a console which can lead to errors.
  drush_shell_exec("find {$from['root']} -type f -name '*.js' ! -name '*.min.js' -exec grep -rl 'console' {} \; | xargs grep -nL 'window.console'");
  $console_results = drush_shell_exec_output();
  if ($console_count = count($console_results)) {
    drush_log(dt('Javascript files which use console but don\'t declare window.console:'), 'warning');
    seed_util_error_print($console_results);
  }

  if (($dpm_count || $console_count) && drush_confirm(dt('Detected some possible code quality issues. Abort sync?'))) {
    return drush_set_error('SEED_SYNC_QUALITY_ABORT', dt('[Quality] Sync aborted.'));
  }
}