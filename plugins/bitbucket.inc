<?php

/**
 * Implementation of hook_seed_user_key_added().
 *
 * Takes a newly added key and pushes it to Bitbucket.org.
 */
function bitbucket_seed_user_key_added($user, $key) {
  $config = seed_get_config(array(
    '/\[USER\]/' => $user,
    '/\[KEY\]/' => urlencode($key)
  ));

  $request = $config['bitbucket_new_key_cmd'];
  $request = str_replace(array("\r", "\r\n", "\n"), '', $request);

  // @todo We're circumventing drush's standard exec calls here. Keys with %
  // in them will get munged by drush_shell_exec's insistence on putting
  // everyting through sprintf and we cann't use drush_op_system as we need
  // output. Can we do this better?
  exec($request . ' 2>&1', $output, $result);

  $json = null;
  foreach ($output as $line) {
    if (stripos($line, 'Bad Request') !== FALSE) {
      return drush_set_error('BITBUCKET_seed_KEY_BAD_REQUEST', dt('[Bitbucket] The service responded with Bad Request, exiting.'));
    }

    if (stripos($line, '"pk"') !== FALSE) {
      $line = '{' . substr(trim($line), 0, -1) . '}';
      $json = json_decode($line);
    }
  }

  if (!$json || !$json->pk) {
    return drush_set_error('BITBUCKET_seed_KEY_BAD_RETURN', dt('[Bitbucket] Could not determine the key ID from the service'));
  }

  $seed_file = '/home/' . $user . '/.ssh/seed';
  $sudo = !is_writable($seed_file) ? 'sudo ' : '';

  drush_shell_exec($sudo . "sh -c 'echo \"{$json->pk}\" > $seed_file'");
  drush_log(dt('[Bitbucket] Pushed SSH key to service.'), 'ok');
}

/**
 * Implementation of hook_seed_user_pre_delete().
 *
 * Removes the key from Bitbucket.org just before the user is deleted.
 */
function bitbucket_seed_user_pre_delete($user) {
  $seed_file = '/home/' . $user . '/.ssh/seed';
  $sudo = !is_readable($seed_file) ? 'sudo ' : '';

  drush_shell_exec($sudo . "cat $seed_file");
  $id = reset(drush_shell_exec_output());

  if (!$id || !is_numeric($id)) {
    return drush_set_error('BITBUCKET_seed_NO_ID', dt("[Bitbucket] Couldn't determine the key ID."));
  }

  $config = seed_get_config(array(
    '/\[KEY\]/' => $id,
  ));

  drush_shell_exec($config['bitbucket_del_key_cmd']);
  $output = drush_shell_exec_output();
  foreach ($output as $line) {
    if (stripos($line, 'Bad Request') !== FALSE) {
      return drush_set_error('BITBUCKET_seed_KEY_BAD_REQUEST', dt('[Bitbucket] The service responded with Bad Request, exiting.'));
    }
  }

  $sudo = !is_writable($seed_file) ? 'sudo ' : '';

  drush_shell_exec($sudo . "rm /home/$username/.ssh/seed");

  drush_log(dt('[Bitbucket] Key was pulled from service'), 'ok');
}

/**
 * Implementation of hook_seed_remote_init().
 */
function bitbucket_seed_remote_init($project) {
  $config = seed_get_config(array(
    '/\[PROJECT\]/' => $project,
  ));

  $status = drush_shell_exec($config['bitbucket_post_create_cmd']);
  if (!$status) {
    drush_log(dt('[Bitbucket] Error running post-remote create command:'), 'error');
    seed_util_error_print(drush_shell_exec_output());
  }
  drush_log(dt('[Bitbucket] Ran post-remote create command'), 'ok');
}
