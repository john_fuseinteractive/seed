<?php

seed_include('project.inc');

/**
 * Command callback for `drush seed-init`.
 */
function drush_seed_init($project_name = NULL) {
  $config = seed_get_config();

  if (empty($project_name)) {
    $project_name = drush_prompt(dt('Project name'));
  }

  $project = new SeedProject($project_name, $_SERVER['USER']);

  // Don't let the user override an existing dir.
  if ($project->dir_exists()) {
    return drush_set_error('SEED_PROJECT_EXISTS', dt('The directory !dir already exists, exiting to prevent any overriding.', array('!dir' => $project->dir)));
  }

  // Present the user with a choice of how to proceed.
  $init = drush_choice(array(
    'clone' => dt('Clone an existing project repository'),
    'create' => dt('Create a new project'),
    'copy' => dt('Copy project from another user'),
  ));

  switch ($init) {
    case 'clone':
      $project->create_dir();
      seed_clone_project($project);
      seed_invoke('seed_clone_complete', $project->dir);
      break;
    case 'create':
      $project->create_dir();
      drush_seed_bare($project, FALSE);
      break;
    case 'copy':
      drush_seed_copy(NULL, NULL, $project_name, $_SERVER['USER']);
      break;
    default:
      return drush_set_error('SEED_PROJECT_BAILED', dt('Creation of project cancelled.'));
  }

  if (drush_get_error()) {
    drush_log(dt('[Project] Aborting due to error.'), 'warning');
    return;
  }

  if (drush_confirm(dt('Symlink the files directory?'))) {
    drush_seed_symlink_files($project_name, $_SERVER['USER']);
  }

  // Add the global database user.
  // Chances are this has been run before but it will fail silently when it
  // sees $config['repo_global_db_user']. Not too concerned about the overhead.
  drush_seed_db_user_add($config['project_global_db_user']);
  $settings_db = $project_name;

  // Add a database for this project for the global user.
  // This is non-optional.
  drush_seed_db_add($project_name, $config['project_global_db_user']);

  // Check to see if we want to create a project database for this user.
  if (drush_confirm(dt('Create database !project_!user?', array('!project' => $project_name, '!user' => $_SERVER['USER'])))) {
    drush_seed_db_add($project_name, $_SERVER['USER']);
    $settings_db = $project_name . '_' . $_SERVER['USER'];
  }

  // Offer to create the settings.php.
  if (drush_confirm(dt('Create a settings.php file with the database !db?', array('!db' => $settings_db)))) {
    // Not particularly happy with this, but seed-init doesn't bootstrap far
    // enough and I'm not sure it's possble to change the effective working
    // directory and escalate bootstrap level.
    drush_shell_exec_interactive('cd ' . $project->dir . ' && drush seed-db-settings ' . $settings_db);
  }

  if (drush_confirm('Create Drush alias?')) {
    seed_add_alias_dev($project_name, $_SERVER['USER']);
  }

  // Handle adding the vhost.
  drush_seed_add($project_name);
}

/**
 * Command callback for `drush seed-bare`.
 *
 * Initialize an empty project and optionally set up Git.
 */
function drush_seed_bare($project_name = NULL, $prompt = TRUE) {
  if (is_string($project_name)) {
    $project = new SeedProject($project_name, $_SERVER['USER']);
  }
  else if (!is_object($project_name) || (is_object($project_name) && get_class($project_name) !== 'SeedProject')) {
    $project_name = drush_prompt(dt('Project name'));
    $project = new SeedProject($project_name, $_SERVER['USER']);
  }
  else {
    $project = $project_name;
  }

  if (!$project->dir_exists()) {
    $project->create_dir();
  }

  if (drush_confirm(dt('Use git?'))) {
    $remote = drush_confirm(dt('Initialize new remote: !remote ?', array('!remote' => $project->config['project_repo_template'])));

    $project->bare_repository($remote);

    if ($remote) {
      seed_invoke('seed_remote_init', $project_name);
    }
  }

  if ($prompt && drush_confirm(dt('Add a virtual host?'))) {
    drush_seed_add($project_name);
  }
}

/**
 * Command callback for `drush seed-delete`.
 */
function drush_seed_delete($project = NULL) {
  $config = seed_get_config();

  if (is_string($project)) {
    $project = new SeedProject($project, $_SERVER['USER']);
  }
  else if (!is_object($project) || (is_object($project) && get_class($project) !== 'SeedProject')) {
    $project = drush_prompt(dt('Project name'));
    $project = new SeedProject($project, $_SERVER['USER']);
  }

  if (drush_confirm(dt('Are you sure you wish to delete !dir?', array('!dir' => $project->dir)))) {
    $project->delete();
  }

  drush_seed_remove($project->name);
}

/**
 * Command callback for `drush seed-symlink-files`.
 */
function drush_seed_symlink_files($project_name = NULL, $project_user = NULL, $target_dir = NULL) {
  $config = seed_get_config();

  if (empty($project_name)) {
    $project_name = drush_prompt(dt('Project name'));
  }

  if (empty($project_user)) {
    $project_user = drush_prompt(dt('Project user'), $_SERVER['user']);
  }

  $project = new SeedProject($project_name, $project_user);

  if (empty($target_dir)) {
    $target_dir = drush_prompt(dt('Target directory'), $project->config['files_directory']);
  }

  if (drush_confirm(dt('Symlink the files directory for !project (user !project_user) to the files directory !target?', array('!project' => $project_name, '!project_user' => $project_user, '!target' => $target_dir)))) {
    $project->symlink_files($target_dir);
  }
}

/**
 * Command callback for `drush seed-copy`.
 */
function drush_seed_copy($from_project_name = NULL, $from_project_user = NULL, $to_project_name = NULL, $to_project_user = NULL) {
  if (empty($from_project_name)) {
    $from_project_name = drush_prompt(dt('"From" project name'));
  }

  $users_have_project = array();
  foreach (seed_util_list_users() as $user) {
    if ($user == $_SERVER['USER']) continue;

    $temp_project = new SeedProject($from_project_name, $user);
    if ($temp_project->dir_exists()) {
      $users_have_project[] = $user;
    }
  }

  if (empty($from_project_user)) {
    $from_project_user = drush_choice($users_have_project, dt('"From" user'));

    if ($from_project_user === FALSE) {
      return drush_set_error('SEED_PROJECT_COPY_NO_USER_SELECTION', dt('No selection made.'));
    }
    else {
      $from_project_user = $users_have_project[$from_project_user];
    }
  }

  if (empty($to_project_name)) {
    $to_project_name = drush_prompt(dt('"To" project name'), $from_project_name);
  }

  if (empty($to_project_user)) {
    $to_project_user = drush_prompt(dt('"To" user'));
  }

  $from_project = new SeedProject($from_project_name, $from_project_user);
  $from_project->check_status();

  $to_project = new SeedProject($to_project_name, $to_project_user);
  $to_project->copy($from_project);
}

/**
 * Command callback for `drush seed-sync`.
 */
function drush_seed_sync($project = NULL) {
  if (empty($project)) {
    $project = drush_prompt(dt('Project name'));
  }

  $alias = drush_sitealias_get_record('@' . $project);
  $sites = $alias['site-list'];

  if (!count($sites)) {
    return drush_set_error('SEED_PROJECT_SYNC_NO_ALIAS', dt('No aliases have been defined for !project.', array('!project' => $project)));
  }

  sort($sites);

  $selection = drush_choice($sites, dt('Where should we sync from?'));
  $from_alias = $sites[$selection];

  if ($selection === FALSE) {
    return drush_set_error('SEED_PROJECT_SYNC_NO_SELECTION', dt('No selection made.'));
  }

  // Destination can't be the same as source.
  unset($sites[$selection]);

  $selection = drush_choice($sites, dt('Where should we sync to?'));
  $to_alias = $sites[$selection];

  if ($selection === FALSE) {
    return drush_set_error('SEED_PROJECT_SYNC_NO_SELECTION', dt('No selection made.'));
  }

  $selection = drush_choice_multiple(array('code' => 'Code', 'files' => 'Files', 'db' => 'Database'), FALSE, dt('What should be synced?'));

  if ($selection === FALSE) {
    return drush_set_error('SEED_PROJECT_SYNC_NO_SELECTION', dt('No selection made.'));
  }

  seed_invoke('seed_pre_sync', $from_alias, $to_alias, $selection);
  // A plugin has raised an error, halt execution.
  if (drush_get_error()) {
    return;
  }

  drush_shell_exec("drush $to_alias cc all");

  if (array_search('code', $selection) !== FALSE) {
    drush_shell_exec_interactive("drush rsync --exclude-files $from_alias $to_alias");
  }

  if (array_search('files', $selection) !== FALSE) {
    $from_files_dir = drush_prompt(dt("From files directory"), 'sites/default/files');
    $to_files_dir = drush_prompt(dt("To files directory"), 'sites/default/files');
    drush_shell_exec_interactive("drush rsync $from_alias:$from_files_dir $to_alias:$to_files_dir");
  }

  if (array_search('db', $selection) !== FALSE) {
    drush_shell_exec_interactive("drush sql-sync $from_alias $to_alias");
  }

  drush_shell_exec("drush $to_alias cc all");

  seed_invoke('seed_post_sync', $from_alias, $to_alias, $selection);

  drush_log(dt('[Project] Seed sync complete'), 'ok');
}

/**
 * Command callback for `drush seed-feature-branch`.
 */
function drush_seed_feature_branch($project_name = NULL, $project_user = NULL, $branch_name = NULL) {
  global $databases;

  if (empty($project_name)) {
    $cwd = array_pop(explode('/', drush_locate_root()));
    $project_name = drush_prompt(dt('Project name'), $cwd);
  }

  if (empty($project_user)) {
    $project_user = drush_prompt(dt('Project user'), $_SERVER['USER']);
  }

  if (empty($branch_name)) {
    $branch_name = drush_prompt(dt('Branch name'));
  }

  $project = new SeedProject($project_name, $project_user);

  $project->check_status();
  $project->create_feature_branch($branch_name);

  $db_spec = _drush_sql_get_db_spec();
  $target_db = $db_spec['database'] . '_' . $branch_name;

  $db_source = drush_choice(array(
    'clone' => 'Clone database ' . $db_spec['database'],
    'load' => 'Load a database dump'
  ));

  if ($db_source == 'clone') {
    drush_seed_db_clone($db_spec['database'], $target_db);
  }
  else if ($db_source == 'load') {
    drush_seed_db_load($db_spec['database'], $project_user, $target_db);
  }

  drush_seed_db_settings($target_db, $db_spec['username'], $db_spec['password']);
}

/**
 * Command callback for `drush seed-merge-branch`.
 */
function drush_seed_merge_branch($project_name = NULL, $project_user = NULL, $branch_name = NULL) {
  if (empty($project_name)) {
    $cwd = array_pop(explode('/', drush_locate_root()));
    $project_name = drush_prompt(dt('Project name'), $cwd);
  }

  if (empty($project_user)) {
    $project_user = drush_prompt(dt('Project user'), $_SERVER['USER']);
  }

  $project = new SeedProject($project_name, $project_user);
  $current_branch = $project->get_branch();

  if (empty($branch_name)) {
    $branch_name = drush_prompt(dt('Branch name'), $current_branch);
  }

  $local_branches = $project->get_branches();
  $filter = 'return $v != "' . $current_branch . '";';
  $local_branches = array_filter($local_branches, create_function('$v', $filter));

  $merge_into = drush_choice($local_branches, dt('Merge into'));

  $status = $project->merge_feature_branch($branch_name, $merge_into);

  if ($status) {
    if (drush_confirm(dt('Delete branch !branch?', array('!branch' => $branch_name)))) {
      $project->delete_feature_branch($branch_name);
    }

    if (drush_confirm(dt('Switch databases?'))) {
      $db_spec = _drush_sql_get_db_spec();
      drush_seed_db_settings(NULL, $db_spec['username'], $db_spec['password']);
      drush_seed_db_delete($project_name . '_' . $branch_name);
    }
  }
  else {
    if ($current_branch !== 'master') {
      $project->switch_branch($current_branch);
    }
  }
}

/**
 * Command callback for `drush seed-db-settings`.
 */
function drush_seed_db_settings($database = NULL, $username = NULL, $password = NULL) {
  $config = seed_get_config();

  $project_name = array_pop(explode('/', drush_locate_root()));
  $project = new SeedProject($project_name, $_SERVER['USER']);

  if (empty($database)) {
    $database = drush_prompt(dt('Database for this project'), $project_name);
  }

  if (empty($username)) {
    $username = drush_prompt(dt('MySQL username to connect to this database'), $config['project_global_db_user']);
  }

  if (empty($password)) {
    $password = drush_prompt(dt('MySQL password to connect to this database'));
  }

  $project->write_db_settings($database, $username, $password);
}

/**
 * Internal function to clone a project from a remote Git repository.
 */
function seed_clone_project($project = NULL) {
  if (is_string($project)) {
    $project = new SeedProject($project, $_SERVER['USER']);
  }
  else if (!is_object($project) || (is_object($project) && get_class($project) !== 'SeedProject')) {
    $project = drush_prompt(dt('Project name'));
    $project = new SeedProject($project, $_SERVER['USER']);
  }

  $repo = drush_prompt(dt('Repository URL'), $project->config['project_repo_template']);

  $project->clone_repository($repo);
}
