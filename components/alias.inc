<?php

seed_include('alias.inc');

/**
 * Command callback for `drush seed-init`.
 */
function drush_seed_add_alias($project = NULL) {
  if (empty($project)) {
    $project = drush_prompt(dt('Project name'));
  }

  $types = array('dev' => 'Development', 'remote' => 'Remote');
  $selection = drush_choice($types, dt('What type of alias is this?'));

  if ($selection != 'Cancelled') {
    $func = "seed_add_alias_$selection";
    if (function_exists($func)) {
      $func($project);
    }
  }
}

/**
 * This is not a command callback, rather an internal method to aid code
 * organization.
 *
 * In particular, this function will receive a project name from the
 * drush_seed_add_alias() function, prompt for additional information and
 * then pass this information to a SeedAlias object.
 */
function seed_add_alias_dev($project) {
  if (empty($user)) {
    $user = drush_prompt('User', $_SERVER['USER']);
  }

  $alias = new SeedAlias($project, $user);

  $name = 'dev.' . $user;

  if ($alias->alias_exists($name)) {
    drush_log(dt('[Alias] The alias @!project.!name already exists', array('!project' => $project, '!name' => $name)), 'warning');
    if (!drush_confirm('Do you wish to override?')) {
      drush_log(dt('[Alias] Cancelled alias creation.'), 'warning');
      return;
    }
  }

  $root_path = drush_prompt(dt('Web root path'), $alias->config['project_directory']);
  $files_path = drush_prompt(dt('Files path'), $root_path . '/sites/default/files');
  $db_path = drush_prompt(dt('Database dump path'), $alias->config['db_dump_directory']);
  $uri = drush_prompt(dt('URI'), $alias->config['server_vhost_template']);

  $alias->dev_alias($root_path, $files_path, $db_path, $uri);
}

/**
 * As above. This passes many collected params to SeedAlias::remote_alias
 * which isn't ideal but it ensures that the method should still be easily
 * testable.
 */
function seed_add_alias_remote($project) {
  $alias = new SeedAlias($project);

  $name = drush_prompt(dt('Give this alias a name (e.g. staging, live)'));

  // Check to see if this alias already exists and give the user to override or
  // prompt for a new name.
  while ($alias->alias_exists($name)) {
    drush_log(dt('The alias !name already exists', array('!name' => $name)), 'warning');
    if (drush_confirm(dt('Do you wish to override?'))) {
      break;
    } else {
      $name = drush_prompt(dt('Give this alias a name (e.g. staging, live)'));
    }
  }

  $uri  = drush_prompt(dt('URI (e.g. mydrupalsite.com)'));
  $root = drush_prompt(dt('Web root path (e.g. /var/www/mydrupalsite)'));
  $path_aliases['%files'] = drush_prompt(dt('Path to files'), $root . '/sites/default/files');

  // remote host prompts
  $remote_host = drush_prompt(dt('Remote host'), $uri);
  $remote_user = drush_prompt(dt('Remote user'));
  $use_ssh_pass = drush_confirm(dt('Use password authentication for SSH? If you choose "no" public key authentication will need to configured.'));

  // database prompts
  $db_driver = drush_prompt(dt('Database driver'), 'mysql');
  $db_host = drush_prompt(dt('Database host'), 'localhost');
  $db_user = drush_prompt(dt('Database user'));
  $db_pass = drush_prompt(dt('Database password'));
  $db_name = drush_prompt(dt('Database name'));

  $allowed_versions = array(6 => 6, 7 => 7);

  $drupal_version = drush_choice($allowed_versions, dt('Drupal version'));
  if (!in_array($drupal_version, $allowed_versions)) {
    return drush_set_error('DRUSH_ALIAS_BAD_VERSION', dt('Only Drupal 6 & 7 are supported.'));
  }

  // path-aliases prompts
  if (!drush_confirm(dt('Is Drush available in the $PATH variable on the remote server?'))) {
    if (drush_confirm(dt('Is Drush available?'))) {
      $path_aliases['%drush'] = drush_prompt(dt('Path to Drush executable'));
    }
  }

  $path_aliases['%dump-dir'] = drush_prompt(dt('Where should databases be kept when syncing?'));

  // command-specific prompts
  $dump_cache = drush_confirm(dt('Sync database cache tables with this install?'));

  $alias->remote_alias(
    $name, $uri, $root,
    $remote_host, $remote_user, $use_ssh_pass,
    $db_driver, $db_host, $db_user, $db_pass, $db_name,
    $drupal_version, $path_aliases, $dump_cache);
}
