<?php

seed_include('db.inc');

/**
 * Command callback for `drush seed-db-user-add`.
 */
function drush_seed_db_user_add($username = NULL) {
  if (empty($username)) {
    $username = drush_prompt(dt('User name'), $_SERVER['USER']);
  }

  $db = new SeedDB();
  $db->user_add($username);
}

/**
 * Command callback for `drush seed-db-user-delete`.
 */
function drush_seed_db_user_delete($username = NULL) {
  if (empty($username)) {
    $username = drush_prompt(dt('User name'));
  }

  $db = new SeedDB();

  if ($username == $db->config['project_global_db_user']) {
    return drush_set_error('SEED_DB_DELETE_GLOBAL_USER', dt('This user cannot be deleted.'));
  }

  if (drush_confirm(dt('Are you sure you want to delete the MySQL user !user?', array('!user' => $username)))) {
    $status = $db->user_delete($username);

    if ($status !== FALSE) {
      drush_seed_db_delete('%_' . $username);
    }
  }
}

/**
 * Command callback for `drush seed-db-add`.
 */
function drush_seed_db_add($project = NULL, $username = NULL) {
  if (empty($project)) {
    $project = drush_prompt(dt('Project name'));
  }

  if (empty($username)) {
    $username = drush_prompt(dt('User name'), $_SERVER['USER']);
  }

  $db = new SeedDB();

  // If we're adding a database for the global users, the database name is just
  // the project name. Otherwise we add the username as a suffix. This
  // organizes database lists nicely.
  $db_name = ($username == $db->config['project_global_db_user']) ? $project : $project . '_' . $username;

  $db->database_add($db_name, $username);
}

/**
 * Command callback for `drush seed-db-delete`.
 */
function drush_seed_db_delete($database = NULL) {
  if (empty($database)) {
    $database = drush_prompt(dt('Database name'));
  }

  $db = new SeedDB();

  $dbs = seed_util_db_matching_pattern($database, $db);

  // We have DB(s) that match.
  if (is_array($dbs) && count($dbs)) {
    // More than one.
    if (count($dbs) > 1) {
      $selection = drush_choice_multiple($dbs, FALSE, dt('Select databases to delete.'));
      if (is_array($selection) && count($selection)) {
        foreach ($selection as $database) {
          $db->database_delete($database);
        }
      }
    }
    // just one.
    else {
      $database = reset($dbs);
      if (drush_confirm(dt('Are you sure you want to delete !db?', array('!db' => $database)))) {
        $db->database_delete($database);
      }
    }
  }
  else {
    return drush_set_error('DRUSH_DB_NONE_FOUND', dt('Could not find database(s) matching "!db".', array('!db' => $database)));
  }
}

/**
 * Command callback for `drush seed-db-dump`.
 */
function drush_seed_db_dump($project = NULL, $username = NULL, $tag = NULL) {
  if (empty($project)) {
    $project = drush_prompt(dt('Project name'));
  }

  if (empty($username)) {
    $username = drush_prompt(dt('User name'), $_SERVER['USER']);
  }

  if (empty($tag)) {
    $tag = drush_prompt(dt('Descriptive tag (Optional)'), NULL, FALSE);
  }

  $config = seed_get_config(array(
    '/\[PROJECT\]/' => $project,
    '/\[USER\]/' => $username,
  ));

  $project_db = NULL;
  if (file_exists($config['project_directory'] . '/sites/default/settings.php')) {
    @require $config['project_directory'] . '/sites/default/settings.php';

    if (!empty($db_url)) {
      $parts = parse_url($db_url);
      $project_db = trim($parts['path'], '/');
    }
    else if (!empty($databases['default']['default'])) {
      $project_db = $databases['default']['default']['database'];
    }
  }

  $db = new SeedDB($project, $username);

  $db_name = drush_prompt(dt('Database name'), !empty($project_db) ? $project_db : $project);

  $db->database_dump($db_name, $username, $tag);
}

/**
 * Command callback for `drush seed-db-load`.
 */
function drush_seed_db_load($project = NULL, $username = NULL, $db_name = NULL) {
  if (empty($project)) {
    $project = drush_prompt(dt('Project name'));
  }

  if (empty($username)) {
    $username = drush_prompt(dt('User name'), $_SERVER['USER']);
  }

  $db = new SeedDB($project, $username);

  if (empty($db_name)) {
    $db_name = ($username == $db->config['project_global_db_user']) ? $project : $project . '_' . $username;
    $db_name = drush_prompt(dt('Load in to database'), $db_name);
  }

  $db_path = $db->config['db_dump_directory'];

  if (!seed_util_dir_exists($db_path)) {
    return drush_set_error('SEED_DB_BACKUP_DIR_NON_EXISTANT', dt('The database backup directory !dir does not seem to exist.', array('!dir' => $db_path)));
  }

  $dumps = glob('{' . $db_path . '/*.gz,' . $db_path . '/*.sql}', GLOB_BRACE);
  $options = array();
  foreach ($dumps as $dump) {
    $options[$dump] = array_pop(explode('/', $dump));
  }

  uasort($options, function($a, $b) use($db_path) {
    return filectime($db_path . '/' . $a) < filectime($db_path . '/' . $b);
  });

  if (drush_confirm(dt('Backup the database first?'))) {
    $db->database_dump($db_name, $username);
  }

  $selection = drush_choice($options, dt('Which DB should be loaded?'));

  if ($selection !== FALSE) {
    $db->database_load($selection, $db_name, $username);
  }
}

/**
 * Command callback for `drush seed-db-clone`.
 */
function drush_seed_db_clone($project = NULL, $target = NULL) {
  if (empty($project)) {
    $project = drush_prompt(dt('Project name'));
  }

  $source = drush_prompt(dt('Source database'), $project);

  if (empty($target)) {
    $target = drush_prompt(dt('New database name'));
  }

  $db = new SeedDB();

  // Check if database exists, create if not
  if (count(seed_util_db_matching_pattern($target, $db))) {
    if (drush_confirm(dt('Target database !target exists. Overwrite?', array('!target' => $target)))) {
      if (drush_confirm(dt('Backup the database first?'))) {
        $db->database_dump($target, $db->config['global_user']);
      }
      $db->database_delete($target);
    }
    else {
      return drush_set_error('SEED_DB_DB_EXISTS', dt('[Database] The database !db already exists.', array('!db' => $target)));
    }
  }

  $db->database_add($target, $db->config['global_user']);
  $db->database_clone($source, $target);
}
