<?php

class SeedUserTest {
  /**
   * Tests the creation and deletion of system users.
   */
  public function testUserCreateAndDelete() {
    $user = new SeedUser('test_user');
    $user->create(FALSE);

    SeedTest::assert(seed_util_dir_exists('/home/test_user'), TRUE);

    $user->delete(TRUE, FALSE);

    // PHP's is_dir() would always return TRUE here. Not sure why but easily
    // worked around as seed_util_dir_exists() uses a shell command to test
    // the existence of a directory.
    SeedTest::assert(seed_util_dir_exists('/home/test_user'), FALSE);
  }

  /**
   * Tests the creation of an SSH key for a user.
   */
  public function testUserAddKey() {
    $user = new SeedUser('test_user');
    $user->create(FALSE);
    $user->add_key('test', 'test.com');
    $key = $user->get_key('test');

    SeedTest::assertContains('test', $key);

    $user->delete(TRUE, FALSE);
  }
}