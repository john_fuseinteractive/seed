<?php

class SeedTest {
  // Counts the number of failing assertions.
  public static $fails = 0;
  // Counts the number of passing assertions.
  public static $passes = 0;

  /**
   * In the event an assertion fails, it will call fail() which can log what
   * happened and which test caused the failure.
   *
   * @param string $message
   */
  public static function fail($message) {
    $bt = debug_backtrace();
    // First shift is the assert* function, second is the caller.
    array_shift($bt);
    $caller = array_shift($bt);
    drush_log($message . ' See ' . $caller['file'] . '@' . $caller['line'], 'error');
    self::$fails++;
  }

  /**
   * In the event an assertion passes, it will call pass() which simply
   * increments the pass counter.
   */
  public static function pass() {
    self::$passes++;
  }

  /**
   * Simple boolean assertion.
   *
   * @param bool $actual The Actual result.
   * @param bool $expected The expected result.
   */
  public static function assert($actual, $expected) {
    if ($actual !== $expected) {
      self::fail('assert failed!');
    }
    else {
      self::pass();
    }
  }

  /**
   * Assert that a file exists in the file system.
   *
   * @param string $file Path to the file.
   */
  public static function assertFileExists($file) {
    if (!file_exists($file)) {
      self::fail('assertFileExists failed!');
    }
    else {
      self::pass();
    }
  }

  /**
   * Assert that a file exists by a pattern.
   *
   * @param string $pattern File path pattern.
   */
  public static function assertFileExistsByPattern($pattern) {
    if (!count(glob($pattern))) {
      self::fail('assertFileExistsByPattern failed!');
    }
    else {
      self::pass();
    }
  }

  /**
   * Assert a string contains another string.
   *
   * @param string $needle The string we're looking for.
   * @param string $haystack The string we're looking in.
   */
  public static function assertContains($needle, $haystack) {
    if (stripos($haystack, $needle) === FALSE) {
      self::fail('assertContains failed!');
    }
    else {
      self::pass();
    }
  }

  /**
   * Assert one value is greater than the other.
   *
   * @param mixed $actual The actual value.
   * @param mixed $expected The expected value.
   */
  public static function assertGreaterThan($actual, $expected) {
    if ($actual <= $expected) {
      self::fail('assetGreaterThan failed!');
    }
    else {
      self::pass();
    }
  }

  /**
   * Assert one value is less than the other.
   *
   * @param mixed $actual The actual value.
   * @param mixed $expected The expected value.
   */
  public static function assertLessThan($actual, $expected) {
    if ($actual >= $expected) {
      self::fail('assetLessThan failed!');
    }
    else {
      self::pass();
    }
  }
}