<?php

/**
 * Seed is a light-weight, minimal configuration script for running an Apache
 * http server in a development environment.
 */

// Load components (command callbacks) and plugins (defined in the .info file)
seed_include('util.inc', 'components');
seed_include('server.inc', 'components');
seed_include('project.inc', 'components');
seed_include('user.inc', 'components');
seed_include('db.inc', 'components');
seed_include('alias.inc', 'components');
seed_load_plugins();

/**
 * Implementation of hook_drush_help().
 */
function seed_drush_help($section) {
  switch ($section) {
    case 'meta:seed:title':
      return dt("Seed development environment commands");
    case 'meta:seed:summary':
      return dt('Start and stop the server, add and remove virtual hosts.');
  }
}

/**
 * Implementation of hook_drush_command().
 */
function seed_drush_command() {
  $items = array();

  $items['seed-commands'] = array(
    'description' => 'Provides help for all seed commands.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-config'] = array(
    'description' => 'Returns a seed configuration setting.',
    'arguments' => array(
      'conf' => 'The configuration variable to return.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-dir'] = array(
    'description' => 'Returns the directory in which seed is installed.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-install'] = array(
    'description' => 'Install the "seed" symlink to /usr/local/bin.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-start'] = array(
    'description' => 'Start the web server.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-stop'] = array(
    'description' => 'Stop the web server.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-restart'] = array(
    'description' => 'Restart the web server.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-status'] = array(
    'description' => 'Display the current status (running/not running) of the server.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-check-config'] = array(
    'description' => 'Check the syntax and content of the generated configuration files. This command also checks to ensure that paths to log files and the DocumentRoot are valid and will disable any virtual hosts which might prevent the server from starting.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-error-log'] = array(
    'description' => 'Display the server-level error log using `tail`.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-list'] = array(
    'description' => 'Display all configured virtual hosts and their mapped DocumentRoot.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-add'] = array(
    'description' => 'Add a virtual host configuration for a project.',
    'arguments' => array(
      'project' => 'The name of the project. You will be prompted for a project name if you do not supply one.',
    ),
    'examples' => array(
      'seed add topsecret' => 'Creates the virtual host configuration for the "topsecret" project of the user running the command.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-remove'] = array(
    'description' => 'Remove a virtual host configuration for a project.',
    'arguments' => array(
      'project' => 'The name of the project. You will be prompted for a project name if you do not supply one.',
    ),
    'examples' => array(
      'seed remove topsecret' => 'Removes the virtual host configuration for the "topsecret" project of the user running the command.',
    ),
    'aliases' => array('seed-rm'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-init'] = array(
    'description' => 'Initializes a project. Optionally, you will be able to create a new project or clone one from a Git repository. This command also walks through the processes of adding a database for the project and setting up a virtual host, as well as optionally symlinking the files directory, creating a user specific database and adding a Drush alias.',
    'arguments' => array(
      'project' => 'The name of the project. You will be prompted for a project name if you do not supply one.',
    ),
    'examples' => array(
      'seed init topsecret' => 'Initializes a project called "topsecret" for the user running the command.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-bare'] = array(
    'description' => 'Initializes an empty project. Optionally initializes the project as a Git repository and adds a remote. Also optionally creates a virtual host. Useful for quickly setting up a non-Drupal project, for example.',
    'arguments' => array(
      'project' => 'The name of the project. You will be prompted for a project name if you do not supply one.',
    ),
    'examples' => array(
      'seed bare topsecret' => 'Creates a project called "topsecret" for the user running the command.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-delete'] = array(
    'description' => 'Deletes a project. Will optionally delete the project\'s virtual host.',
    'arguments' => array(
      'project' => 'The name of the project. You will be prompted for a project name if you do not supply one.',
    ),
    'examples' => array(
      'seed delete topsecret' => 'Deletes the "topsecret" project for the user running the command.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-copy'] = array(
    'description' => 'Copy a project from one user to another. This command will also warn if the source project is not up to date with the remote version control.',
    'arguments' => array(
      'source project' => 'The name of the project to copy from. You will be prompted for a project name if you do not supply one.',
      'source user' => 'The owner of the project being copied from. You will be prompted for a user name if you do not supply one.',
      'destination project' => 'The name of the project to copy to. You will be prompted for a project name if you do not supply one.',
      'destination user' => 'The owner of the project being copied to. You will be prompted for a user name if you do not supply one.',
    ),
    'examples' => array(
      'seed copy topsecret jane classified john' => 'Copy Jane\'s "topsecret" project to John\'s "classified" project.',
    ),
    'aliases' => array('seed-cp'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-symlink-files'] = array(
    'description' => 'Symlinks a files directory to another directory.',
    'arguments' => array(
      'project' => 'The name of the project. You will be prompted for a project name if you do not supply one.',
      'user' => 'The owner of the project. You will be prompted for a user name if you do not supply one.',
      'directory' => 'The directory which will be symlinked to. You will be prompted for a directory if you do not supply one, however the default option is the "files_directory" value from the seed.info file.',
    ),
    'examples' => array(
      'seed symlink-files topsecret jane /storage/files/topsecreet' => 'Symlinks sites/default/files to /storage/files/topsecret for Jane\'s "topsecret" project.',
    ),
    'aliases' => array('seed-sf'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-add-alias'] = array(
    'description' => 'Add a Drush alias. You will be presented with the option of creating a development alias (a local project) or a remote alias (for example, a live site).',
    'arguments' => array(
      'project' => 'The name of the project. You will be prompted for a project name if you do not supply one.',
    ),
    'examples' => array(
      'seed add-alias topsecret' => 'Adds an alias for the "topsecret" project.',
    ),
    'aliases' => array('seed-aa'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-sync'] = array(
    'description' => 'Synchronize projects between aliases. You will be able to select the source, destination and type (code, files, database) of the sync via prompts.',
    'arguments' => array(
      'project' => 'The name of the project. You will be prompted for a project name if you do not supply one.',
    ),
    'examples' => array(
      'seed sync topsecret' => 'Lists the aliases for the "topsecret" project.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-feature-branch'] = array(
    'description' => 'Creates a project feature branch.',
    'arguments' => array(
      'project' => 'The name of the project. You will be prompted for a project name if you do not supply one.',
      'user' => 'The owner of the project. You will be prompted for a user name if you do not supply one.',
      'branch' => 'The name of the branch to create. You will be prompted for a branch name if you do not supply one.',
    ),
    'examples' => array(
      'seed feature-branch topsecret jane a-cool-feature' => 'Creates the branch "a-cool-feature" for Jane\'s "topsecret" project.',
    ),
    'aliases' => array('seed-fb'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
  );

  $items['seed-merge-branch'] = array(
    'description' => 'Merges a project feature branch in to master.',
    'arguments' => array(
      'project' => 'The name of the project. You will be prompted for a project name if you do not supply one.',
      'user' => 'The owner of the project. You will be prompted for a user name if you do not supply one.',
      'branch' => 'The name of the branch to merge. You will be prompted for a branch name if you do not supply one.',
    ),
    'examples' => array(
      'seed merge-branch topsecret john a-cool-feature' => 'Merges the branch "a-cool-feature" in to the master branch of John\'s "topsecret" project.',
    ),
    'aliases' => array('seed-mb'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
  );

  $items['seed-db-settings'] = array(
    'description' => 'Updates the database settings for a project.',
    'arguments' => array(
      'database' => 'The name of the database. You will be prompted for a database name if you do not supply one.',
      'username' => 'The user name to use when connecting to the database. You will be prompted for a user name if you do not supply one.',
      'password' => 'The password to use when connecting to the database. You will be prompted for a password if you do not supply one.',
    ),
    'examples' => array(
      'seed db-settings topsecret john h8rd2gu3ss!' => 'Rewrite the database configuration in the settings.php file to connect to the "topsecret" database with the username "john" and the password "h8rd2gu3ss!".',
    ),
    'aliases' => array('seed-dbs'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
  );

  $items['seed-user-add'] = array(
    'description' => 'Adds a system user. You may optionally grant the user sudo rights, as well as create a corresponding database user and create an SSH key.',
    'arguments' => array(
      'user' => 'The name of the user to create. You will be prompted for a user name if you do not supply one.',
    ),
    'examples' => array(
      'seed user-add jane' => 'Creates the user "jane" on the system.',
    ),
    'aliases' => array('seed-useradd'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-user-delete'] = array(
    'description' => 'Deletes a system user. You will optionally be able to backup the user\'s home directory before it is deleted and delete any corresponding database user.',
    'arguments' => array(
      'user' => 'The name of the user to delete. You will be prompted for a user name if you do not supply one.',
    ),
    'examples' => array(
      'seed user-delete john' => 'Deletes "john" from the system.',
    ),
    'aliases' => array('seed-userdel'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-user-add-key'] = array(
    'description' => 'Adds an SSH key for a user with the intention of using this key to authenticate with a hosted version control system (Github, Bitbucket, etc). In an attempt to play nicely with existing keys and configuration, you will be prompted to give the new key an alias and specify the host the key is used on. See the example provided.',
    'arguments' => array(
      'user' => 'The user for which to create an SSH key. You will be prompted for a user name if you do not supply one.',
    ),
    'examples' => array(
      'seed user-add-key john' => 'Creates an SSH key for John. If, when prompted, you enter the alias "Github" and the hostname "github.com", this command will create, in /home/john/.ssh/, the files Github, Github.pub and use the public key in Github.pub when connecting to github.com.',
    ),
    'aliases' => array('seed-key'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-db-user-add'] = array(
    'description' => 'Creates a database user.',
    'arguments' => array(
      'user' => 'The user to create. You will be prompted for a user name if you do not supply one.',
    ),
    'examples' => array(
      'seed db-user-add john' => 'Creates the database user "john".',
    ),
    'aliases' => array('seed-dbua'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-db-user-delete'] = array(
    'description' => 'Deletes a database user and optionally their databases.',
    'arguments' => array(
      'user' => 'The user to delete. You will be prompted for a user name if you do not supply one.',
    ),
    'examples' => array(
      'seed db-user-delete jane' => 'Deletes the database user "jane" and then lists any of their suffixed databases for optional deletion.',
    ),
    'aliases' => array('seed-dbud'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-db-add'] = array(
    'description' => 'Adds a database.',
    'arguments' => array(
      'project' => 'The name of the database to add. You will be prompted for a database name if you do not supply one.',
      'user' => 'The user of the database. You will be prompted for a user name if you do not supply one.',
    ),
    'examples' => array(
      'seed db-add topsecret jane' => 'Creates the database "topsecret_jane".',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-db-delete'] = array(
    'description' => 'Deletes a database.',
    'arguments' => array(
      'database name' => 'The name of the database to delete. Use of the "%" wildcard is permitted. You will be prompted for a database name if you do not supply one.',
    ),
    'examples' => array(
      'seed db-delete topsecret%' => 'List the databases with a name beginning with "topsecret" and optionally delete them.',
      'seed db-delete classified' => 'Delete the database "classified".',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-db-dump'] = array(
    'description' => 'Dumps a project database to a gzipped .sql file.',
    'arguments' => array(
      'project' => 'The name of the project for which the dump file is to be generated. You will be prompted for a project name if you do not supply one.',
      'user' => 'The user of the database. You will be prompted for a user name if you do not supply one.',
      'tag' => 'A description to be appended to the resulting dump file. You will be prompted for a description if you do not supply one.',
    ),
    'examples' => array(
      'seed db-dump topsecret john classified' => 'Dump a database for the "topsecret" project and the user John, and append the description "classified".',
    ),
    'aliases' => array('seed-dbd'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-db-load'] = array(
    'description' => 'Loads a project database from a (optionally gzipped) .sql file.',
    'arguments' => array(
      'project' => 'The name of the project where .sql files are stored. You will be prompted for a project name if you do not supply one.',
      'user' => 'The user suffix of the database. You will be prompted for a user name if you do not supply one.',
      'database' => 'The name of the database to load in to. You will be prompted for a database name if you do not supply one.',
    ),
    'examples' => array(
      'seed db-load topsecret john topsecret' => 'Presents a list of files which can be loaded in to the "topsecret" database.',
    ),
    'aliases' => array('seed-dbl'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-db-clone'] = array(
    'description' => 'Clone a project database (structure and data) to a new database.',
    'arguments' => array(
      'project' => 'The name of the project. You will be prompted for a project name if you do not supply one.',
      'target' => 'The name of the database to clone to. This will be created for you. You will be prompted for a database name if you do not supply one.',
    ),
    'examples' => array(
      'seed db-clone topsecret classified' => 'Creates the database "classified" and populates it with the structure and data from the "topsecret" database.',
    ),
    'aliases' => array('seed-dbc'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['seed-test'] = array(
    'description' => 'Callback for testing functionality.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 * Command callback for `drush seed-commands`.
 */
function drush_seed_commands($command = NULL) {
  $all_commands = drush_get_commands();

  if ($command && $command != 'commands') {
    $seed_command = 'seed-' . $command;
    if (isset($all_commands[$seed_command])) {
      $choice = $seed_command;
    }
  }
  else {
    $command_keys = array_keys($all_commands);
    $seed_commands = array();
    foreach ($command_keys as $key) {
      if (stripos($key, 'seed-') === FALSE || $all_commands[$key]['is_alias'] || $key === 'seed-commands') {
        continue;
      }

      $seed_commands[$key] = substr($key, 5);
    }
    $choice = drush_choice($seed_commands, 'Display help for command:');
  }

  if ($choice) {
    drush_invoke_process('@self', 'help', array($choice));
  }
}

/**
 * Command callback for `drush seed-config`.
 */
function drush_seed_config($var) {
  $config = seed_get_config();

  drush_print($config[$var]);
}

/**
 * Command callback for `drush seed-dir`.
 */
function drush_seed_dir() {
  drush_print(dirname(__FILE__));
}

/**
 * Include a file from the seed includes directory.
 */
function seed_include($filename, $directory = 'includes') {
  $file = dirname(__FILE__) .'/' . $directory . '/'. $filename;
  if (file_exists($file)) {
    include_once $file;
  }
}

/**
 * Load the config file and include any plugins that are enabled.
 */
function seed_load_plugins() {
  $config = seed_get_config();

  foreach ($config['plugins'] as $plugin) {
    seed_include($plugin . '.inc', 'plugins');
  }
}

/**
 * Command callback for `drush seed-test`.
 *
 * A basic test suite runner. Tests are under the ./tests directory and are
 * written in classes named Seed<Example>Test. Any methods in those classes
 * prefixed with "test" (i.e. testAnExample()) are run automatically. Results
 * are stored by the static SeedTest class (see ./tests/base.inc) and
 * reported once all tests are finished.
 *
 * These tests should cover basic Seed functionality and where possible
 * should not require input, however there are certain situations, such as
 * where functionality is handled by system functions (i.e. adduser) that
 * user input is required.
 *
 * To run only certain tests, pass them in as arguments.
 */
function drush_seed_test() {
  $args = func_get_args();

  seed_include('base.inc', 'tests');
  if ($handle = opendir(dirname(__FILE__) . '/tests')) {
    while (false !== ($entry = readdir($handle))) {
      if ($entry != "." && $entry != ".." && $entry != 'base.inc') {
        seed_include($entry, 'tests');

        $name = array_shift(explode('.', $entry));

        // We have arguments but this test isn't in them, so continue.
        if (count($args) && !in_array($name, $args)) {
          continue;
        }

        $class = 'Seed' . ucfirst($name) . 'Test';
        $instance = new $class;

        $class_methods = get_class_methods($instance);
        foreach ($class_methods as $method_name) {
          if (substr($method_name, 0, 4) === 'test') {
            drush_log('[Test] Running ' . $class . '::' . $method_name, 'ok');
            $instance->$method_name();
          }
        }
      }
    }
    closedir($handle);
  }
  drush_log('---------------------', 'ok');
  $status = (SeedTest::$fails === 0) ? 'ok' : 'warning';
  drush_log('[Test] ' . SeedTest::$fails . ' Failures', $status);
  drush_log('[Test] ' . SeedTest::$passes . ' Passes', $status);
}

/**
 * An invocation system similar to Drupal's hooks. Command callbacks can call
 * seed_invoke('seed_a_hook', $a). Any plugin implementing the hook (i.e.
 * <PluginName>_seed_a_hook($a)) will be called automatically. For example
 * we are shipping 1.0 with Bitbucket functionality which responds to user SSH
 * keys being created by pushing them to Bitbucket and deleting them when the
 * user is deleted.
 *
 * @todo Does Drush already implement something like this, can we use that?
 * @todo If not, we're going to have to add more seed_invoke() calls
 * throughout the codebase.
 *
 * @param string $hook The hook to invoke.
 */
function seed_invoke($hook) {
  static $funcs;
  if (!isset($funcs)) {
    $all = get_defined_functions();
    $funcs = $all['user'];
  }
  $args = func_get_args();
  unset($args[0]);
  foreach ($funcs as $function) {
    if (stripos($function, $hook) !== FALSE) {
      call_user_func_array($function, $args);
    }
  }
}

/**
 * Loads, parses and returns the config file.
 *
 * @param mixed[] $replacements Map of replacements to make. Allows tokens to
 * be used in configuration values.
 *
 * @return A map of configuration names and values.
 */
function seed_get_config($replacements = array()) {
  static $config;
  if (!isset($config)) {
    // Check a couple of locations for a seed config file.
    foreach (array('/config/seed.info', '/seed.info') as $file) {
      if (file_exists(dirname(__FILE__) . $file)) {
        $temp = parse_ini_file(dirname(__FILE__) . $file);
        // Static variable replacement, only needs to happen once
        foreach ($temp as $key => &$value) {
          if (is_string($value)) {
            preg_match_all('/(\[THIS.([a-zA-Z0-9\/\.-_]+?)\])/', $value, $matches);
            foreach ($matches[0] as $idx => $target) {
              $value = str_replace($target, $temp[$matches[2][$idx]], $value);
            }
          }
        }
        $config = $temp;
        // Once we've found one file, we're done.
        break;
      }
    }

    // Couldn't load any files, exit.
    if (empty($config)) {
      drush_log(dt('Could not load the global config file.'), 'error');
      exit;
    }
  }

  // Dynamic variable replacement, could happen multiple times as a result of
  // functions calling other functions.
  if (count($replacements)) {
    $arr1 = $arr2 = array();
    foreach ($replacements as $pattern => $replacement) {
      $arr1[] = $pattern;
      $arr2[] = $replacement;
    }
    $temp = $config;
    foreach ($temp as $key => $value) {
      $temp[$key] = preg_replace($arr1, $arr2, $value);
    }
    return $temp;
  }
  else {
    return $config;
  }
}

/**
 * Get a static instance of the server object.
 *
 * @return Static SeedServer instance.
 */
function seed_get_server() {
  static $server;
  if (!isset($server)) {
    seed_include('server.inc');
    $server = new SeedServer();
  }
  return $server;
}

/**
 * Get an instance of a HTTP configuration file object.
 *
 * @return SeedHttpdConfFile instance.
 */
function seed_get_conf_file() {
  seed_include('conf-file.inc');
  $conf_file = new SeedHttpdConfFile();
  return $conf_file;
}

/**
 * Command callback for `drush seed-install`.
 */
function drush_seed_install() {
  $message = <<<DT
This command will attempt to install a symbolic link that will allow you to use
`seed` instead of `drush seed`.  In order for this to work, the specified
install path must be in your shell's PATH.
Install path
DT;

  $path = drush_prompt($message, '/usr/local/bin');
  if (is_dir($path) && is_writable($path)) {
    $source = dirname(__FILE__) .'/seed.drush';
    $destination = $path .'/seed';
    if (symlink($source, $destination)) {
      drush_log("[Installed] $destination", 'ok');
    }
  }
}
