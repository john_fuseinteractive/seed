# Seed Config

Place your config files here to override the base ones that come bundled with seed.

The following files are accepted:

seed.info
httpd.tpl.php
vhost.tpl.php
