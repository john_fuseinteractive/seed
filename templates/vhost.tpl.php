<?php
/**
 *
 * Available Variables:
 * - host
 * - port
 * - uri
 * - conf_path
 * - base_path
 * - log_path
 * - doc_root
 * - conf_file
 */
?>
#
# Generated by drush-server.
#
<VirtualHost *:<?php print $port; ?>>
  ServerName <?php print $host . "\n"; ?>
  ServerAlias www.<?php print $host . "\n"; ?>

  DocumentRoot <?php print $doc_root . "\n"; ?>
  <Directory <?php print $doc_root; ?>>
    AllowOverride all
    Order deny,allow
    Allow from all
  </Directory>

  ErrorLog <?php print $log_directory; ?>/error.log

  # Possible values include: debug, info, notice, warn, error, crit,
  # alert, emerg.
  LogLevel warn

  CustomLog <?php print $log_directory; ?>/access.log combined
</VirtualHost>

<IfModule ssl_module>
  <VirtualHost *:443>
    ServerName <?php print $host . "\n"; ?>
    SSLEngine on

    SSLCertificateFile      /etc/apache2/dev.crt
    SSLCertificateKeyFile   /etc/apache2/dev.key

    DocumentRoot <?php print $doc_root . "\n"; ?>
    <Directory <?php print $doc_root; ?>>
      AllowOverride all
      Order allow,deny
      Allow from all
    </Directory>

    ErrorLog <?php print $log_directory; ?>/error.log
    LogLevel warn

    CustomLog <?php print $log_directory; ?>/access.log combined
  </VirtualHost>
</IfModule>
