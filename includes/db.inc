<?php

seed_include('base.inc');

/**
 * This class abstracts a database connection, as opposed to a single object.
 *
 * @todo Refactor this in to separate classes for users and databases.
 */
class SeedDB extends SeedBase {
  protected $attributes = array(
    'dbh' => NULL,
    'config' => NULL,
    'test' => FALSE,
  );

  /**
   * Creates and stores a new database handle.
   */
  public function __construct($name = NULL, $user = NULL, $test = FALSE) {
    $this->config = seed_get_config(array(
      '/\[PROJECT\]/' => $name,
      '/\[USER\]/' => $user
    ));
    $this->dbh = new PDO('mysql:dbname=mysql;host=' . $this->config['mysql_host'], $this->config['mysql_user'], $this->config['mysql_pass']);
    $this->test = $test;
  }

  /**
   * In the event of one of the methods in this class encountering a database
   * error, they will call this function to output an error message. Saves a
   * lot of error handling boilerplate.
   *
   * @param PDOStatement $sth PDO Statement object where an error was raised.
   * @param string $error_type Type of error.
   */
  protected function sql_error(PDOStatement $sth, $error_type = 'SEED_DB_GENERIC_ERROR') {
    $error = $sth->errorInfo();
    return drush_set_error($error_type, dt('[Database] The following error occurred: !error', array('!error' => $error[2])));
  }

  /**
   * Fetches the MySQL users. It will statically cache the user list and return
   * that list for any subsequent calls to this method.
   *
   * @param bool $fresh Always load a fresh user list.
   * @return array An array of users.
   */
  public function get_users($fresh = FALSE) {
    static $users;
    if ($this->test) {
      $fresh = TRUE;
    }
    if (!isset($users) || $fresh === TRUE) {
      $users = $this->dbh->query('SELECT user FROM user')->fetchAll(PDO::FETCH_COLUMN);
    }

    return $users;
  }

  /**
   * Checks to see if a MySQL user exists.
   *
   * @param string $user Name of the user.
   * @param bool $fresh Load users afresh.
   * @return bool Whether or not the user exists.
   */
  public function user_exists($user, $fresh = FALSE) {
    $users = $this->get_users($fresh);
    return (array_search($user, $users) !== FALSE);
  }

  /**
   * Add a user to MySQL.
   *
   * @param string $user Name of the user to add.
   */
  public function user_add($user) {
    if ($this->user_exists($user)) {
      if ($user != $this->config['global_user']) {
        return drush_set_error('SEED_DB_USER_EXISTS', dt('[Database] MySQL user !user already exists.', array('!user' => $user)));
      } else {
        return;
      }
    }

    $sth = $this->dbh->prepare("CREATE USER ?@'localhost' IDENTIFIED BY ?");
    if(!$sth->execute(array($user, $user))) {
      return $this->sql_error($sth, 'SEED_DB_USER_CREATE_FAILED');
    }

    $sth = $this->dbh->prepare("GRANT USAGE ON *.* TO ?@'localhost' IDENTIFIED BY ? WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0");
    if(!$sth->execute(array($user, $user))) {
      return $this->sql_error($sth, 'SEED_DB_USER_GRANT_FAILED');
    }

    $this->dbh->query("FLUSH PRIVILEGES");

    drush_log(dt('[Database] Added MySQL user !user', array('!user' => $user)), 'ok');
  }

  /**
   * Delete a user from MySQL.
   *
   * @param string $user name of the user to delete.
   */
  public function user_delete($user) {
    if (!$this->user_exists($user)) {
      return drush_set_error('SEED_DB_NO_USER_EXISTS', dt('[Database] MySQL user !user does not exist.', array('!user' => $user)));
    }

    $sth = $this->dbh->prepare("DROP USER ?@'localhost'");
    if(!$sth->execute(array($user))) {
      return $this->sql_error($sth, 'SEED_DB_USER_DELETE_FAILED');
    }

    drush_log(dt('[Database] Deleted MySQL user !user', array('!user' => $user)), 'ok');
  }

  /**
   * Add a database.
   *
   * @param string $name Name of the database to add.
   * @param string $user Name of the user to grant access to.
   */
  public function database_add($name, $user) {
    if (count(seed_util_db_matching_pattern($name, $this))) {
      return drush_set_error('SEED_DB_DB_EXISTS', dt('[Database] The database !db already exists.', array('!db' => $name)));
    }

    // PDO doesn't allow parameters for table and column names. I do not like this.
    $sth = $this->dbh->prepare("CREATE DATABASE $name");
    if(!$sth->execute()) {
      return $this->sql_error($sth, 'SEED_DB_CREATE_FAILED');
    }

    // As above.
    $sth = $this->dbh->prepare("GRANT ALL PRIVILEGES ON $name.* TO ?@'localhost' WITH GRANT OPTION");
    if(!$sth->execute(array($user))) {
      drush_log($name, 'error');
      return $this->sql_error($sth, 'SEED_DB_DB_GRANT_FAILED');
    }

    $this->dbh->query("FLUSH PRIVILEGES");

    drush_log(dt('[Database] Created MySQL database !db for user !user', array('!db' => $name, '!user' => $user)), 'ok');
  }

  /**
   * Delete a database.
   *
   * @param string $name Name of the database to delete.
   */
  public function database_delete($name) {
    if (!count(seed_util_db_matching_pattern($name, $this))) {
      return drush_set_error('SEED_DB_NO_DB_EXISTS', dt('[Database] The database !db does not exist.', array('!db' => $name)));
    }

    // As above.
    $sth = $this->dbh->prepare("DROP DATABASE $name");
    if(!$sth->execute()) {
      return $this->sql_error($sth, 'SEED_DB_DB_DELETE_FAILED');
    }

    drush_log(dt('[Database] Deleted MySQL database !db', array('!db' => $name)), 'ok');
  }

  /**
   * Dump a database.
   *
   * @param string $name Name of the database to dump.
   * @param string $user
   */
  public function database_dump($name, $user, $tag = '') {
    if (stripos($name, '_') !== FALSE) {
      $project = array_shift(explode('_', $name));
    }
    else {
      $project = $name;
    }

    $db_path = $this->config['db_dump_directory'];
    if (!seed_util_dir_exists($db_path)) {
      if ($user != $_SERVER['USER']) {
        $status = seed_util_dir_create($db_path, TRUE, $user);
      }
      else {
        $status = seed_util_dir_create($db_path);
      }

      if (!$status) {
        return drush_set_error('SEED_DB_BACKUP_DIR_UNCREATED', dt('[Database] The backup directory !dir could not be created.', array('!dir' => $db_path)));
      }
    }

    $output_name = ($tag) ? $name . '-' . $tag : $name;
    $file_path = $db_path . "/$output_name-" . date('Y-m-d-H-i-s') . '.sql';

    drush_shell_exec("mysqldump -u {$this->config['mysql_user']} -p{$this->config['mysql_pass']} $name > $file_path");
    drush_shell_exec("gzip $file_path");

    drush_log(dt('[Database] Backed up to !path', array('!path' => $file_path . '.gz')), 'ok');
  }

  /**
   * Loads a database dump in to a database.
   *
   * @param string $file Path of the file to load.
   * @param string $name Name of the database to load in to.
   * @param string $user User of the database being loaded in to.
   */
  public function database_load($file, $name, $user) {
    $this->database_delete($name);
    $this->database_add($name, $user);

    // Check to see if it's a gzipped file.
    $gz = stripos($file, '.gz');
    if ($gz !== FALSE) {
      $sql_file = substr($file, 0, $gz);
      drush_shell_exec("gunzip $file");
    }
    else {
      $sql_file = $file;
    }

    drush_shell_exec("mysql -u {$this->config['mysql_user']} -p{$this->config['mysql_pass']} $name < $sql_file");

    // If it was a gzip file, zip it again.
    if ($gz !== FALSE) {
      drush_shell_exec("gzip $sql_file");
    }

    drush_log(dt('[Database] Back up !path loaded in to database !db', array('!path' => $file, '!db' => $name)), 'ok');
  }

  /**
   * Clones a database.
   *
   * @param $source Name of the source database.
   * @param $target Name of the target database. Should exist already.
   */
  public function database_clone($source, $target) {
    drush_log(dt('[Database] Cloning !source to database !target', array('!source' => $source, '!target' => $target)), 'ok');
    drush_shell_exec("mysqldump -u {$this->config['mysql_user']} -p{$this->config['mysql_pass']} $source | mysql -u {$this->config['mysql_user']} -p{$this->config['mysql_pass']} $target");
  }
}
