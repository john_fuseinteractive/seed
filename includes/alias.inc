<?php

seed_include('base.inc');

/**
 * Handles the management of Drush alias files.
 */
class SeedAlias extends SeedBase {
  protected $attributes = array(
    'project' => NULL,
    'user' => NULL,
    'config' => NULL,
  );

  /**
   * Constructor to set up the alias.
   *
   * @param string $project Name of the project.
   * @param string $user Name of the user the alias belongs to.
   */
  public function __construct($project, $user = NULL) {
    $this->project = $project;
    $this->user = $user;
    $this->config = seed_get_config(array(
      '/\[PROJECT\]/' => $project,
      '/\[USER\]/' => $user
    ));
  }

  /**
   * Create a local development alias.
   *
   * @param string $root_path Path to the webroot.
   * @param string $files_path Path to the site's files directory.
   * @param string $db_path Path to where database content is stored.
   * @param string $uri URI of the alias.
   */
  public function dev_alias($root_path, $files_path, $db_path, $uri) {
    $aliases = $this->read();
    $name = 'dev.' . $this->user;

    $aliases[$name] = array(
      'root' => $root_path,
      'uri' => $uri,
      'path-aliases' => array(
        '%dump-dir' => $db_path,
        '%files' => $files_path,
      ),
      'command-specific' => array(
        'sql-dump' => array(
          'result-file' => $db_path . '/' . $this->project . "-' . date('Y-m-d-H-i-s') . '.sql",
          'gzip' => TRUE,
          'yes' => TRUE,
        ),
      ),
    );

    $this->write($aliases);
    drush_log(dt('[Alias] Created new alias: @!project.!name', array('!project' => $this->project, '!name' => $name)), 'ok');
  }

  /**
   * Create a remote alias.
   *
   * @param string $name Name of the alias.
   * @param string $uri URI of the alias.
   * @param string $root Path to the webroot.
   * @param string $remote_host The remote host the site is on.
   * @param string $remote_user User to log in to the remote host.
   * @param bool $use_ssh_pass Whether or not to use SSH password auth.
   * @param string $db_driver Database driver to use.
   * @param string $db_host Host of the database.
   * @param string $db_user Database user.
   * @param string $db_pass Database user's password.
   * @param string $db_name Database name.
   * @param int $drupal_version Version of Drupal that is being run.
   * @param array $path_aliases Paths to use on the remote host.
   * @param bool $dump_cache Include cache tables for dumps of this site.
   */
  public function remote_alias($name, $uri, $root, $remote_host, $remote_user, $use_ssh_pass, $db_driver, $db_host, $db_user, $db_pass, $db_name, $drupal_version, $path_aliases, $dump_cache) {
    $aliases = $this->read();

    // Standard stuff.
    $aliases[$name] = array(
      'uri' => $uri,
      'root' => $root,
      'remote-host' => $remote_host,
      'remote-user' => $remote_user,
      'path-aliases' => $path_aliases,
    );

    if ($use_ssh_pass) {
      $aliases[$name]['ssh-options'] = ' -o PasswordAuthentication=yes';
    }

    // Database definition depends on the Drupal version.
    switch ($drupal_version) {
      case 6:
        $db_key = 'db-url';
        $db_value = $db_driver . '://' . $db_user . ':' . $db_pass . '@' . $db_host . '/' . $db_name;
        break;
      case 7:
        $db_key = 'databases';
        $db_value = array(
          'default' => array(
            'default' => array(
              'driver' => $db_driver,
              'username' => $db_user,
              'password' => $db_pass,
              'port' => '',
              'host' => $db_host,
              'database' => $db_name,
            ),
          ),
        );
        break;
    }

    // Add the database definition.
    $aliases[$name][$db_key] = $db_value;

    // Command specific.
    $aliases[$name]['command-specific'] = array(
      'sql-sync' => array(
        'no-cache' => $dump_cache,
      ),
    );

    $this->write($aliases);
    drush_log(dt('[Alias] Created new alias: @!project.!name', array('!project' => $this->project, '!name' => $name)), 'ok');
  }

  /**
   * Check to see if an alias exists.
   *
   * @param string $name Name of the alias.
   * @return bool Whether or not the alias exists.
   */
  public function alias_exists($name) {
    $aliases = $this->read();

    return isset($aliases[$name]);
  }

  /**
   * Reads the file being referenced by this instance.
   *
   * @return array The aliases.
   */
  protected function read() {
    $aliases = array();

    $alias_file = $this->config['alias_path'] . '/' . strtolower($this->project) . '.aliases.drushrc.php';
    if (is_readable($alias_file)) {
      $contents = file_get_contents($alias_file);
      $contents = str_replace('<?php', '', $contents);
      eval($contents);
    } else {
      drush_log(dt('[Alias] Could not read the alias file !file. This is not a fatal error however it may result in aliases not being created.', array('!file' => $alias_file)), 'warning');
    }

    return $aliases;
  }

  /**
   * Writes the file being referenced by this instance.
   *
   * @param array $aliases Array of aliases to write to the file.
   */
  protected function write($aliases) {
    $contents = "<?php\n\n";
    foreach ($aliases as $name => $alias) {

      // This date() will get intepreted by the eval() in this->read() so
      // replace the result-file again.
      if (stripos($name, 'dev') !== FALSE) {
        $alias['command-specific']['sql-dump']['result-file'] = $alias['path-aliases']['%dump-dir'] . '/' . $this->project . "-' . date('Y-m-d-H-i-s') . '.sql";
      }

      $contents .= '$aliases[\"' . $name . '\"] = ';
      $contents .= var_export($alias, TRUE);
      $contents .= ";\n";
    }

    // Turn escaped single quotes in to just a single quote, then turn single
    // quotes in to escaped double quotes. Also escape $.
    $contents = str_replace(array("\'", '\'', '$'), array('\'', '\"', '\$'), $contents);

    $alias_file = $this->config['alias_path'] . '/' . strtolower($this->project) . '.aliases.drushrc.php';
    $sudo = !is_writable($alias_file) ? 'sudo ' : '';

    drush_op_system($sudo . "sh -c 'echo \"$contents\" > $alias_file'");
  }

  /**
   * Delete the file being referenced by this instance.
   */
  public function delete() {
    $alias_file = $this->config['alias_path'] . '/' . strtolower($this->project) . '.aliases.drushrc.php';

    $sudo = !is_writable($alias_file) ? 'sudo ' : '';
    drush_op_system($sudo . 'rm ' . $alias_file);
  }
}