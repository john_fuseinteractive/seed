<?php

seed_include('base.inc');

/**
 * Handles the management of projects.
 */
class SeedProject extends SeedBase {
  protected $attributes = array(
    'name' => NULL,
    'user' => NULL,
    'config' => NULL,
    'dir' => NULL,
  );

  /**
   * Stores the name of the project and the user it belongs to, as well as
   * an instance of the configuration with the project and user tokens
   * replaced.
   *
   * @param string $name Name of the project.
   * @param string $user Name of the user the project belongs to.
   */
  public function __construct($name, $user) {
    $this->name = str_replace('/', '', $name);
    $this->user = $user;

    $this->config = seed_get_config(array(
      '/\[PROJECT\]/' => $name,
      '/\[USER\]/' => $user
    ));

    // We access this a lot, so create a handy shortcut.
    $this->dir = $this->config['project_directory'];
  }

  /**
   * Check to see if the project directory exists.
   *
   * @return bool Whether or not the directory exists.
   */
  public function dir_exists() {
    return seed_util_dir_exists($this->dir);
  }

  /**
   * Create the project directory.
   */
  public function create_dir() {
    if (!$this->dir_exists()) {
      seed_util_dir_create($this->dir);
      drush_log(dt('[Project] Successfully created directory !dir', array('!dir' => $this->dir)), 'ok');
    }
    else {
      return drush_set_error('SEED_PROJECT_DIR_ERROR', dt('[Project] Unable to create directory !dir, already exists.', array('!dir' => $this->dir)));
    }
  }

  /**
   * Delete the project directory.
   */
  public function delete_dir() {
    if (!$this->dir_exists()) {
      return drush_set_error('SEED_PROJECT_DIR_ERROR', dt('[Project] Unable to delete directory !dir, does not exist.', array('!dir' => $this->dir)));
    }
    else {
      drush_shell_exec('rm -rf ' . $this->dir);
      drush_log(dt('[Project] Successfully deleted directory !dir', array('!dir' => $this->dir)), 'ok');
    }
  }

  /**
   * Clone a repository in to the project directory.
   *
   * @param string $repo URI of the repository to clone.
   */
  public function clone_repository($repo) {
    drush_log(dt('[Project] Cloning repository !repo', array('!repo' => $repo)), 'ok');
    drush_log(dt('[Project] This could take awhile depending on the size of the repository...'), 'ok');

    $status = drush_shell_exec_interactive('git clone ' . $repo . ' ' . $this->dir);
    if (!$status) {
      return drush_set_error('SEED_PROJECT_CLONE_ERROR', dt('[Project] Cloning respository !repo failed.', array('!repo' => $repo)));
    }

    drush_log(dt('[Project] Cloning repository complete'), 'ok');
  }

  /**
   * Create a bare git repository in the project directory.
   *
   * @param bool $remote Set a remote for the repository.
   */
  public function bare_repository($remote) {
    $status = drush_shell_cd_and_exec($this->dir, 'git init .');
    if (!$status) {
      return drush_set_error('SEED_PROJECT_GIT_ERROR', dt('[Project] Unable to initialize a Git repository in !dir.', array('!dir' => $this->dir)));
    }
    drush_log(dt('[Project] Initialized git repository'), 'ok');

    if ($remote) {
      $url = $this->config['project_repo_template'];

      $status = drush_shell_exec($this->config['repo_init_cmd']);
      if (!$status) {
        drush_log(dt('[Project] Could not initialize new remote:'), 'error');
        seed_util_error_print(drush_shell_exec_output());
      }
      drush_log(dt('[Project] Initialized git remote'), 'ok');

      $status = drush_shell_cd_and_exec($this->dir, 'git remote add origin ' . $url);
      if (!$status) {
        drush_log(dt('[Project] Could not add remote:'), 'error');
        seed_util_error_print(drush_shell_exec_output());
      }
      drush_log(dt('[Project] Added remote !remote', array('!remote' => $url)), 'ok');
    }
  }

  /**
   * Check to see if the current branch for two projects are the same.
   *
   * @param SeedProject $compare The project to compare with.
   *
   * @return bool Whether or not the branches are the same.
   */
  public function compare_branches(SeedProject $compare) {
    drush_shell_cd_and_exec($this->dir, 'echo $(git branch | grep "*" | sed "s/* //")');
    $this_branch = reset(drush_shell_exec_output());

    drush_shell_cd_and_exec($compare->dir, 'echo $(git branch | grep "*" | sed "s/* //")');
    $compare_branch = reset(drush_shell_exec_output());

    return $this_branch == $compare_branch;
  }

  /**
   * Attempts to see if the current branch is behind the remote.
   *
   * @return bool Whether or not the branch is up to date.
   */
  public function check_status() {
    $branch = $this->get_branch();

    drush_shell_cd_and_exec($this->dir, 'git remote show origin');
    $target_str = "$branch pushes to $branch (up to date)";
    $uptodate = FALSE;
    foreach (drush_shell_exec_output() as $line) {
      if (stripos($line, $target_str) !== FALSE) {
        $uptodate = TRUE;
      }
    }

    if (!$uptodate) {
      drush_log(dt('[Project] !dir looks to be behind the remote branch', array('!dir' => $this->dir)), 'warning');
    }

    return $uptodate;
  }

  /**
   * Get the project's current Git branch.
   *
   * @return string The current branch.
   */
  public function get_branch($current = TRUE) {
    drush_shell_cd_and_exec($this->dir, 'git branch');
    $output = drush_shell_exec_output();

    $branch = NULL;
    $branches = array();
    foreach ($output as $line) {
      if (stripos($line, '* ') !== FALSE) {
        $branch = substr($line, 2);
        $branches[$branch] = $branch;
      }
      else {
        $line = trim($line);
        $branches[$line] = $line;
      }
    }

    return ($current) ? $branch : $branches;
  }

  /**
   * Get all local Git branches for the project.
   *
   * @return array Array of branches.
   */
  public function get_branches() {
    return $this->get_branch(FALSE);
  }

  /**
   * Switch a project's Git Branch.
   *
   * @param string $branch Branch to switch to.
   */
  public function switch_branch($branch) {
    drush_shell_cd_and_exec($this->dir, 'git checkout ' . $branch);
  }

  /**
   * Copy the file structure from another project.
   *
   * @param SeedProject $original The project to copy files from.
   */
  public function copy(SeedProject $original) {
    $status = drush_shell_exec('cp -rfa ' . $original->dir . ' ' . $this->dir);
    if (!$status) {
      drush_log(dt('[Project] Could not copy files to the directory !dir:', array('!dir' => $this->dir)), 'error');
      seed_util_error_print(drush_shell_exec_output());
    }

    drush_shell_exec('sudo chmod -R g+w ' . $this->dir);
    drush_shell_exec('sudo chown -R ' . $this->user . ' ' . $this->dir);

    drush_log(dt('[Project] Copied !dir', array('!dir' => $this->dir)), 'ok');
  }

  /**
   * Symlink the files directory of a project to the files of another project.
   *
   * @param string $target The files directory that will be symlinked to.
   */
  public function symlink_files($target) {
    $local_dir = $this->dir . '/sites/default/files';
    if (seed_util_dir_exists($local_dir)) {
      return drush_set_error('SEED_PROJECT_FILES_EXIST', dt('[Project] Files directory exists already, aborting symlink.'));
    }

    if (!seed_util_dir_exists($target)) {
      if (!seed_util_dir_create($target)) {
        return drush_set_error('SEED_PROJECT_NO_TARGET_FILES_DIR', dt('[Project] Could not create the directory !target.', array('!target' => $target)));
      }
    }

    $status = drush_shell_exec('ln -s ' . $target . ' ' . $local_dir);
    if (!$status) {
      return drush_set_error('SEED_PROJECT_SYMLINK_ERROR', dt('[Project] Cannot symlink !dir to !target.', array('!dir' => $local_dir, '!target' => $target)));
    }
    drush_log(dt('[Project] Symlinked !dir to !target', array('!dir' => $local_dir, '!target' => $target)), 'ok');
  }

  /**
   * Delete a project directory.
   */
  public function delete() {
    drush_shell_exec('rm -rf ' . $this->dir);
    drush_log(dt('[Project] !dir has been deleted', array('!dir' => $this->dir)), 'ok');
  }

  /**
   * Creates a new Git branch for a project.
   *
   * @param string $branch New Git branch name.
   */
  public function create_feature_branch($branch) {
    drush_shell_cd_and_exec($this->dir, 'git checkout -b ' . $branch . ' master');
  }

  /**
   * Attempts to merge a feature branch in to the master branch.
   *
   * @param string $branch Name of the branch to merge.
   * @return bool Whether or not the merge command was successful.
   */
  public function merge_feature_branch($branch, $merge_into) {
    $result = drush_shell_exec_interactive('cd ' . $this->dir . ' && git checkout ' . $merge_into . ' && git pull && git merge --no-ff ' . $branch);

    if (!$result) {
      drush_log(dt('[Project] Resolve the previous errors before attempting to merge !branch.', array('!branch' => $branch)), 'error');
    }

    return $result;
  }

  /**
   * Attempts to delete a feature branch.
   *
   * @param string $branch Name of the branch to delete.
   * @return bool Whether or not the branch was deleted.
   */
  public function delete_feature_branch($branch) {
    $result = drush_shell_cd_and_exec($this->dir, 'git branch -D ' . $branch);

    if (!$result) {
      drush_log(dt('[Project] Branch !branch could not be deleted.', array('!branch' => $branch)), 'error');
    }

    return $result;
  }

  /**
   * Attempts to set the database settings in a settings.php file. If the file
   * does not exist, drupal_rewrite_settings() will attempt to create one based
   * on "default.settings.php".
   *
   * @param string $database Name of the database for the project.
   * @param string $username Username to use to connect to the database.
   * @param string $password Password to use to connect to the database.
   */
  public function write_db_settings($database, $username, $password) {
    if (!function_exists('drupal_override_server_variables')) {
      return drush_set_error('SEED_PROJECT_NO_SETTINGS', dt('[Project] Function drupal_override_server_variables() does not exist, cannot create settings.php. Is this a Drupal 7 project?'));
    }

    $db_spec = array(
      'database' => $database,
      'username' => $username,
      'password' => $password,
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    );

    $settings['databases'] = array(
      'value'    => array('default' => array('default' => $db_spec)),
      'required' => TRUE,
    );

    require_once $this->dir . '/includes/install.inc';
    try {
      drupal_override_server_variables();
      drupal_rewrite_settings($settings);
    } catch (Exception $e) {
      drush_log($e->getMessage(), 'error');
    }
  }
}
