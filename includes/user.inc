<?php

seed_include('base.inc');

/**
 * Handles the representation of system-level users.
 */
class SeedUser extends SeedBase {
  protected $attributes = array(
    'name' => NULL,
    'config' => NULL,
  );

  /**
   * Constructor function. Will load an instance of the configuration with user
   * variables replaced by this user.
   *
   * @param string $name Name of the user being represented by this instance.
   */
  public function __construct($name) {
    $this->name = $name;

    $this->config = seed_get_config(array(
      '/\[USER\]/' => $user
    ));
  }

  /**
   * Creates a new users. Hands off to the system-level `adduser` function to
   * do the heavy lifting. Also adds the user to groups as appropriate.
   *
   * @param bool $sudo Whether or not to grant this user sudo rights.
   */
  public function create($sudo = FALSE) {
    drush_log(dt('[User] Handing off to system adduser public function ...'), 'ok');

    $added = drush_shell_exec_interactive('sudo adduser ' . $this->name);
    if (!$added) {
      return drush_set_error('SEED_USER_ADD_FAILED', dt('[User] Could not add user !user.', array('!user' => $this->name)));
    }

    // Add the user to the global group for more conveient permissions.
    drush_shell_exec('sudo usermod -g ' . $this->config['global_group'] . ' ' . $this->name);
    drush_shell_exec('sudo chmod -R g+w /home/' . $this->name);

    if ($sudo) {
      // Ubuntu 12.04 uses the sudo group
      drush_shell_exec('sudo usermod -a -G sudo ' . $this->name);
    }

    drush_log(dt('[User] Successfully added user !user.', array('!user' => $this->name)), 'ok');
  }

  /**
   * Deletes a user. Hands off to the system level `deluser` function to do the
   * heavy lifting.
   *
   * @param bool $delete_home Whether or not to delete the home directory.
   * @param bool $backup_home Whether or not to backup the home directory.
   */
  public function delete($delete_home, $backup_home) {
    $args = '';
    if ($delete_home) {
      $args .= '--remove-home';
      if ($backup_home) {
        $args .= ' --backup-to /home/' . $this->config['global_user'];
      }
    }

    drush_shell_exec_interactive('sudo deluser ' . $args . ' ' . $this->name);
  }

  /**
   * Add an SSH key for the user.
   *
   * @param string $alias Alias for the key, which will also be used for the
   * file name.
   * @param string $host Host the key is intended for.
   */
  public function add_key($alias, $host) {
    if (!seed_util_dir_exists('/home/' . $this->name . '/.ssh')) {
      if (!seed_util_dir_create('/home/' . $this->name . '/.ssh', TRUE, $this->name)) {
        return drush_set_error('SEED_USER_SSH_DIR_NOT_CREATED', dt('[User] Could not create .ssh directory for user !user.', array('!user' => $this->name)));
      }
    }

    $alias_file = '/home/' . $this->name . '/.ssh/' . $alias;
    $sudo = !is_writable($alias_file) ? 'sudo ' : '';

    drush_log(dt('[User] Handing off to ssh-keygen ...'), 'ok');
    drush_shell_exec_interactive($sudo . "ssh-keygen -f $alias_file -C '$alias'");

    // Set up a ssh config file entry for this host.
    $config = <<<CONFIG
Host $host
 IdentityFile /home/$this->name/.ssh/$alias
CONFIG;

    $config_file = '/home/' . $this->name . '/.ssh/config';
    $sudo = !is_writable($config_file) ? 'sudo ' : '';

    drush_shell_exec($sudo . "sh -c 'echo \"$config\" > $config_file'");
    drush_log(dt('[User] SSH key created for user !user.', array('!user' => $this->name)), 'ok');

    // If a user doesn't own the key then publickey auth is going to fail.
    drush_shell_exec('sudo chown -R ' . $this->name . ':' . $this->config['global_group'] . ' /home/' . $this->name . '/.ssh');
  }

  /**
   * Fetches a user's SSH key.
   *
   * @param string $alias Alias of the key, used for the file name.
   * @return string The user's key.
   */
  public function get_key($alias) {
    $key_file = '/home/' . $this->name . '/.ssh/' . $alias . '.pub';

    if (!file_exists($key_file)) {
      return drush_set_error('SEED_USER_KEY_FILE_GONE', dt('[User] Could not locate key file for user !user, alias !alias.', array('!user' => $this->name, '!alias' => $alias)));
    }

    $sudo = !is_readable($key_file) ? 'sudo ' : '';

    drush_shell_exec($sudo . "cat $key_file");
    $key = reset(drush_shell_exec_output());

    return $key;
  }
}