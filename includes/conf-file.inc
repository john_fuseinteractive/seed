<?php

class SeedConfFile {
  public $filepath;
  public $template_name;

  public $base_path;
  public $conf_path;
  public $log_path;

  public function __construct($filepath, $template) {
    $this->filepath = $filepath;
    $this->template_name = $template;

    $server = seed_get_server();
    $this->base_path = $server->base_path;
    $this->conf_path = $server->conf_path;
    $this->log_path = $server->log_path;
    $this->user = $_SERVER['USER'];

    $this->load();
  }

  public function get_template() {
    $command = drush_get_command();
    $override_file = $command['path'] .'/config/'. $this->template_name;
    if (file_exists($override_file)) {
      return $override_file;
    }
    else {
      $default_file = $command['path'] .'/templates/'. $this->template_name;
      return file_exists($default_file) ? $default_file : FALSE;
    }
  }

  public function load() {
    $this->parse();
  }

  protected function parse() {}

  protected function get_template_variables() {
    $variables = get_object_vars($this);
    return $variables;
  }

  public function save($path = NULL) {
    $variables = $this->get_template_variables();
    $content = $this->render($variables);
    $destination = (!$path) ? $this->filepath : $path;

    if (!drush_mkdir(dirname($destination))) {
      return drush_set_error('DRUSH_SERVER_DIR_ERROR',
        dt('Unable to create directory !dir.', array('dir' => dirname($destination))));
    }
    file_put_contents($destination, $content);
  }

  protected function render($variables = array()) {
    $template = $this->get_template();

    if (!$template) {
      return drush_set_error('DRUSH_SERVER_TEMPLATE_NOT_FOUND',
        dt('Unable to locate template !template.', array('!template' => $this->template_name)));
    }

    extract($variables);
    ob_start();
    include $template;
    $content = ob_get_clean();
    return $content;
  }

  public function delete() {
    return unlink($this->filepath);
  }

  public function exists() {
    return file_exists($this->filepath);
  }
}

class SeedHttpdConfFile extends SeedConfFile {
  public $host = 'localhost';
  public $ports = array();
  public $php_path = 'libexec/apache2/libphp5.so';

  public function __construct() {
    $server = seed_get_server();
    $filepath = $server->conf_path .'/httpd.conf';
    parent::__construct($filepath, 'httpd.tpl.php');
  }

  protected function parse() {
    if (!file_exists($this->filepath)) {
      return;
    }

    $content = file_get_contents($this->filepath);

    // Find the Listen directives for the ports.
    preg_match_all('@^\s*Listen\s+([0-9]+)\s*$@m', $content, $matches);
    if (!empty($matches[1])) {
      $this->ports = $matches[1];
    }

    // ServerName localhost
    preg_match_all('@^\s*ServerName\s+[\'"]?([a-zA-Z0-9.-_]+)[\'"]?\s*$@m', $content, $matches);
    if (!empty($matches[1][0])) {
      $this->host = $matches[1][0];
    }

    // LoadModule php5_module libexec/apache2/libphp5.so
    preg_match_all('@^\s*LoadModule\s+php5_module\s+([a-zA-Z0-9\/.-_]+)\s*$@m', $content, $matches);
    if (!empty($matches[1][0])) {
      $this->php_path = $matches[1][0];
    }
  }

  protected function get_template_variables() {
    $variables = parent::get_template_variables();

    $server = seed_get_server();
    $vhosts = $server->get_vhosts();
    $ports = array();
    foreach ($vhosts as $key => $vhost) {
      $ports[] = $vhost->port;
    }

    $variables['ports'] = array_unique($ports);
    $variables['vhosts'] = $vhosts;

    return $variables;
  }
}

class SeedVhostConfFile extends SeedConfFile {
  public $uri;
  public $host;
  public $port;
  public $doc_root;
  public $error_log;
  public $access_log;

  public function __construct($uri) {
    $config = seed_get_config();

    $this->uri = $uri;
    $this->host = parse_url($uri, PHP_URL_HOST);
    $this->port = $config['server_port'];
    $this->name = array_shift(explode('.', $this->host));

    $server = seed_get_server();
    $filepath = "{$server->conf_path}/sites/{$this->host}.conf";
    $this->disablepath = "{$server->conf_path}/disabled/{$this->host}.conf";

    parent::__construct($filepath, 'vhost.tpl.php');

    $seed_config = seed_get_config(array(
      '/\[PROJECT\]/' => $this->name,
      '/\[USER\]/' => $this->user
    ));

    $this->log_directory = $seed_config['log_directory'];
  }

  protected function parse() {
    if (!file_exists($this->filepath)) {
      return;
    }

    $content = file_get_contents($this->filepath);

    // <VirtualHost *:3000>
    preg_match_all('@^\s*<VirtualHost\s+\*:([0-9]+)\s*$@m', $content, $matches);
    if (!empty($matches[1][0])) {
      $this->port = $matches[1][0];
    }

    // ServerName localhost
    preg_match_all('@^\s*ServerName\s+[\'"]?([a-zA-Z0-9.-_]+)[\'"]?\s*$@m', $content, $matches);
    if (!empty($matches[1][0])) {
      $this->host = $matches[1][0];
    }

    // DocumentRoot
    preg_match_all('@^\s*DocumentRoot\s+[\'"]?([a-zA-Z0-9.\/\-_]+)[\'"]?\s*$@m', $content, $matches);
    if (!empty($matches[1][0])) {
      $this->doc_root = $matches[1][0];
    }

    // ErrorLog
    preg_match_all('@^\s*ErrorLog\s+[\'"]?([a-zA-Z0-9.\/\-_]+)[\'"]?\s*$@m', $content, $matches);
    if (!empty($matches[1][0])) {
      $this->error_log = $matches[1][0];
    }

    // CustomLog
    preg_match_all('@^\s*CustomLog\s+[\'"]?([a-zA-Z0-9.\/\-_]+)[\'"]?\s*([a-zA-Z0-9.\/\-_]+)\s*$@m', $content, $matches);
    if (!empty($matches[1][0])) {
      $this->access_log = $matches[1][0];
    }
  }

  public function disable() {
    parent::save($this->disablepath);
    if (!drush_get_error()) {
      parent::delete();
      $httpd_conf = seed_get_conf_file();
      $httpd_conf->save();
    }
  }

  public function save() {
    parent::save();
    $httpd_conf = seed_get_conf_file();
    $httpd_conf->save();
  }

  public function delete() {
    parent::delete();
    $httpd_conf = seed_get_conf_file();
    $httpd_conf->save();
  }
}
